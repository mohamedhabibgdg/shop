<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{
    protected $fillable=[ 'name', 'image', 'price', 'old_price', 'description' ];
    protected $appends=['path_image'];

    public function user(){ return $this->belongsTo(User::class); }

    public function getPathImageAttribute(){ return url('uploads/products/'.$this->image); }

    public function carts(){
        return $this->belongsToMany(User::class,'carts')->withPivot(['qty','id'])->using(Cart::class);
    }
}
