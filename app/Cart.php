<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Cart extends Pivot {
    protected $table="carts";
    protected $fillable=['qty'];
    protected $with=['product'];
    protected $attributes=[
        'qty','id'
    ];
    public function product(){ return $this->belongsTo(Product::class);}
    public function user(){ return $this->belongsTo(User::class);}

}
