<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password','image','role'
    ];
//    protected $guarded=[''];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function products(){ return $this->hasMany(Product::class); }

    public function carts(){
        return $this->belongsToMany(User::class,'carts')->withPivot(['qty','id'])->using(Cart::class);
    }
}
