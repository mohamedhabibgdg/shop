<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller{

    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    public function index()
    {
        return view('home');
    }

    public function myCart(){
        $carts=auth()->user()->carts()->paginate();
        return view('mycarts',compact('carts'));
    }
    public function products(){
        $products=Product::paginate();
        return view('products',compact('products'));
    }
}
