<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function imageUpload($folder,$input='image'){
        $request=request();
        if ($request->hasFile('image')){
            $image=$request->file('image');
            $imageName=uniqid('products-').'.'.$image->getClientOriginalExtension();
            $image->storeAs('products', $imageName);
            return $imageName;
        }
        return "image.jpg";
    }
}
