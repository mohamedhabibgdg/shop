<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(){
        $categories=Category::paginate();
        return view('category.index',compact('categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(CreateCategoryRequest $request){
        $validated=$request->validated();
        Category::create($validated);
        return back()->withSuccess('Category Created Successfully!');
    }

    public function show(Category $category)
    {
        return view('category.show',compact('category'));
    }

    public function edit(Category $category)
    {
        return view('category.edit',compact('category'));
    }

    public function update(UpdateCategoryRequest $request, Category $category){
        $validated=$request->validated();
        $category->update($validated);
        return back()->withSuccess('Category Updated Successfully!');
    }

    public function destroy(Category $category){
        $category->delete();

        return back()->withSuccess('Category Deleted Successfully!');
    }
}
