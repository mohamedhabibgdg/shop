<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Http\Requests\CreateCartRequest;
use App\Http\Requests\UpdateCartRequest;
use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller{

    public function index(){

        $carts=auth()->user()->carts()->paginate();
        return view('cart.index',compact('carts'));
    }

    public function store(Product $product){
        $product->carts()->attach(auth()->id(),[ 'qty'=>1 ]);
        return back()->withSuccess('Cart Created Successfully!');
    }

    public function update(UpdateCartRequest $request, Cart $cart){
        $cart->update($request->validated());
        return back()->withSuccess('Cart Updated Successfully!');
    }

    public function destroy(Cart $cart){
        $cart->delete();
        return back()->withSuccess('Cart Deleted Successfully!');
    }
}
