<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(){
        $products=Product::paginate();
        return view('product.index',compact('products'));
    }

    public function create(){
        return view('product.create');
    }

    public function store(CreateProductRequest $request){
        $validated=$request->validated();
        $validated['image']=$this->imageUpload('products');
        auth()->user()->products()->create($validated);
        return back()->withSuccess('Product Created Successfully!');
    }

    public function show(Product $product)
    {
        return view('product.show',compact('product'));
    }

    public function edit(Product $product)
    {
        return view('product.edit',compact('product'));
    }

    public function update(UpdateProductRequest $request, Product $product){
        $validated=$request->validated();
        if ($request->hasFile('image')){
            $validated['image']=$this->imageUpload('products');
            unlink(public_path($product->path_image));
        }
        $product->update($validated);
        return back()->withSuccess('Product Updated Successfully!');
    }

    public function destroy(Product $product){
        $product->delete();

        return back()->withSuccess('Product Deleted Successfully!');
    }
}
