<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest{

    public function authorize(){
        return auth()->check() && auth()->user()->role===1;
    }

    public function rules(){
        return [
            'name'=>['required'],
            'image'=>['required','image'],
            'price'=>['required','numeric'],
            'category_id'=>['required','numeric'],
            'old_price'=>['sometimes:numeric:min:1'],
            'description'=>['sometimes:string'],
        ];
    }
}
