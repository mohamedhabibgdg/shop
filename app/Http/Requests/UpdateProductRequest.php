<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest{

    public function authorize() { return auth()->check() && auth()->user()->role===1; }

    public function rules(){
        return [
            'name'=>['sometimes'],
            'image'=>['sometimes:image'],
            'price'=>['sometimes:numeric'],
            'category_id'=>['sometimes:numeric'],
            'old_price'=>['sometimes:numeric:min:1'],
            'description'=>['sometimes:string'],
        ];
    }
}
