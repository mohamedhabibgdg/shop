@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if ($products->count()>0)
                @foreach ($products as $product)
                    <div class="col-md-4 mt-3">
                        <div class="card">
                            <img class="card-img-top" src="{{$product->path_image}}" alt="">
                            <div class="card-body">
                                <h4 class="card-title">{{$product->name}}</h4>
                                <p class="card-text">{{$product->description}}</p>
                            </div>
                        </div>
                        <div class="card-footer border-0">
                            <form action="{{route('add.carts',$product)}}" method="post">
                                @csrf
                                <button type="submit" class="btn btn-sm btn-success">Add To Cart</button>
                            </form>
                        </div>
                    </div>
                @endforeach
                {{$products->links()}}
            @else
                <div class="alert text-center alert-info w-100" role="alert">
                    <strong>no Cart</strong>
                </div>
            @endif

        </div>
    </div>
@endsection
