@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h4 class="card-title">Create</h4>
                        <form class="w-100" action="{{route('categories.store')}}"  method="post">
                            @csrf
                            <div class="form-group">
                                <label for="name">name</label>
                                <input type="text" value="{{old('name')}}" class=" @error('name') is-invalid @enderror form-control" name="name" id="name" required aria-describedby="helpIdname" placeholder="name">
                                @error('name')
                                <small id="helpIdname" class="form-text text-muted">{{ $message }}</small>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
