@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-sm btn-primary" href="{{route('categories.create')}}">@lang('main.create')</a>
            </div>
            <div class="col-md-12 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{__('main.categories')}}</h4>
                        @if ($categories->count()>0)
                            <table class="table table-sm table-bordered">
                                <thead class="thead-default">
                                <tr>
                                    <th>#</th>
                                    <th>@lang('main.name')</th>
                                    <th>@lang('main.edit')</th>
                                    <th>@lang('main.delete')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($categories as $category)
                                    <tr>
                                        <td scope="row">{{++$loop->index}}</td>
                                        <td>{{$category->name}}</td>
                                        <td>
                                            <a href="{{route('categories.edit',$category)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a>
                                        </td>
                                        <td>
                                            <form action="{{route('categories.destroy',$category)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                            {{$categories->links()}}
                        @else
                            <div class="alert text-center alert-info" role="alert">
                                <strong>@lang('main.no-categories')</strong>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
