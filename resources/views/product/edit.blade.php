@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h4 class="card-title">Update {{$product->name}}</h4>
                        <form class="w-100" action="{{route('products.update',$product)}}" enctype="multipart/form-data" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">name</label>
                                <input type="text" value="{{$product->name}}" class=" @error('name') is-invalid @enderror form-control" name="name" id="name" required aria-describedby="helpIdname" placeholder="name">
                                @error('name')
                                <small id="helpIdname" class="form-text text-muted">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="price">price</label>
                                <input type="number" min="0" required value="{{$product->price}}" step="any" class=" @error('price') is-invalid @enderror form-control" name="price" id="price" required aria-describedby="helpIdprice" placeholder="price">
                                @error('price')
                                <small id="helpIdprice" class="form-text text-muted">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="old_price">old price</label>
                                <input type="number" min="0" value="{{$product->old_price}}" step="any" class=" @error('old_price') is-invalid @enderror form-control" name="old_price" id="old_price" required aria-describedby="helpIdold_price" placeholder="old_price">
                                @error('old_price')
                                <small id="helpIdold_price" class="form-text text-muted">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="image">image</label>
                                <input type="file" step="any" class=" @error('image') is-invalid @enderror form-control" name="image" id="image" required aria-describedby="helpIdimage" placeholder="image">
                                <div>
                                    <img height="100" width="100" src="{{$product->path_image}}" alt="">
                                </div>
                                @error('image')
                                <small id="helpIdold_price" class="form-text text-muted">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="description">description</label>
                                <textarea required class=" @error('image') is-invalid @enderror form-control" name="description" id="description"  placeholder="description">{{$product->description}}</textarea>
                                @error('image')
                                <small id="helpIdold_price" class="form-text text-muted">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="category_id">category</label>
                                <select class="form-control" name="category_id" id="category_id">
                                    <option></option>
                                    @foreach (\App\Category::all() as $category)
                                        <option {{($product->category_id && $product->category_id===$category->id)?'selected':''}} value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-warning">Update</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
