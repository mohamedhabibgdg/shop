@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-sm btn-primary" href="{{route('products.create')}}">@lang('main.create')</a>
            </div>
            <div class="col-md-12 mt-3">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{__('main.products')}}</h4>
                        @if ($products->count()>0)
                            <table class="table table-sm table-bordered">
                                <thead class="thead-default">
                                <tr>
                                    <th>#</th>
                                    <th>@lang('main.name')</th>
                                    <th>@lang('main.price')</th>
                                    <th>@lang('main.old_price')</th>
                                    <th>@lang('main.image')</th>
                                    <th>@lang('main.edit')</th>
                                    <th>@lang('main.delete')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td scope="row">{{++$loop->index}}</td>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->price}}</td>
                                        <td>{{$product->old_price}}</td>
                                        <td><img width="100" height="50" src="{{url('uploads/products'.$product->image)}}" alt="{{$product->name}}"></td>
                                        <td>
                                            <a href="{{route('products.edit',$product)}}" class="btn btn-sm btn-warning">@lang('main.edit')</a>
                                        </td>
                                        <td>
                                            <form action="{{route('products.destroy',$product)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger">@lang('main.delete')</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
                            {{$products->links()}}
                        @else
                            <div class="alert text-center alert-info" role="alert">
                                <strong>@lang('main.no-products')</strong>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
