@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @if ($carts->count()>0)
                @foreach ($carts as $product)
                    <div class="col-md-12 mt-3">
                        <div class="card">
                            <div class="card-header">{{$product->name}}</div>
                            <div class="card-body">
                                <form id="cart{{$product->id}}" action="{{route('update.carts',$product->pivot->id)}}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="qty">qty</label>
                                        <input onchange="this.submit()" type="number" min="1" value="{{$product->pivot->qty}}" class="form-control" name="qty" id="qty" aria-describedby="helpIdqty" placeholder="qty">
                                    </div>
                                </form>
                                <form id="cart{{$product->id}}" action="{{route('delete.carts',$product->pivot->id)}}" method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-danger">delete</button>
                                </form>

                            </div>
                        </div>
                    </div>
                @endforeach
                {{$carts->links()}}
            @else
                <div class="alert text-center alert-info w-100" role="alert">
                    <strong>no Cart</strong>
                </div>
            @endif

        </div>
    </div>
@endsection
