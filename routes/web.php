<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () { return view('welcome'); });
Auth::routes(['verify'=>true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/myCart', 'HomeController@myCart')->name('myCart');
Route::get('/show/products', 'HomeController@products')->name('products');
Route::post('/add/carts/{product}', 'cartController@store')->name('add.carts');
Route::post('/qty/{cart}', 'cartController@update')->name('update.carts');
Route::post('delete/qty/{cart}', 'cartController@destroy')->name('delete.carts');

Route::resource('products', 'ProductController');
Route::resource('categories', 'CategoryController');

